#!/bin/bash

# This should only be used if the environment is not already running

CODE_DIR=/var/work/moodle
DOCKER_DIR=/var/work/docker-moodle/
#WEB_PORT=12000

export MOODLE_DOCKER_WWWROOT=/var/work/moodle
export MOODLE_DOCKER_DB=mysql
#export MOODLE_DOCKER_WEB_PORT="0.0.0.0:$WEB_PORT"
cd "$DOCKER_DIR"
cp config.docker-template.php "$MOODLE_DOCKER_WWWROOT"/config.php
echo -e "\nStarting containers..."
bin/moodle-docker-compose up -d

echo -e "\nWaiting for database to start..."
bin/moodle-docker-wait-for-db

echo -e "\nInstalling XDebug from PECL..."
bin/moodle-docker-compose exec webserver pecl install xdebug

echo -e "\nGenerating and injecting XDebug config..."
read -r -d '' conf <<'EOF'
; Settings for Xdebug Docker configuration
xdebug.mode=debug
xdebug.start_with_request=yes
xdebug.client_port=9003
; xdebug.remote_handler=dbgp
xdebug.idekey=VSCODE
xdebug.discover_client_host=false
xdebug.client_host=host.docker.internal
xdebug.log=/var/log/xdebug.log
EOF
bin/moodle-docker-compose exec webserver bash -c "echo '$conf' >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini"

echo -e "\nEnabling XDebug and restarting webserver..."
bin/moodle-docker-compose exec webserver docker-php-ext-enable xdebug
bin/moodle-docker-compose restart webserver

echo -e "\nLocal environment started. Changes made to files located here will be reflected live:\n\n\t$MOODLE_DOCKER_WWWROOT"
